import { Document } from "mongoose";

export interface ViewInterface extends Document {
  uuid: string;
  user: number;
  episode_uid: number;
  begin_time: number;
  end_time: number;
  created_at: Date;
}

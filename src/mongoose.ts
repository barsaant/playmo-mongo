import mongoose, { ConnectOptions, Connection, Model } from "mongoose";
import { ViewSchema } from "./schemas";
import { ViewInterface } from "./types";

export class ConnectDB {
  db: Connection;
  ViewModel: Model<ViewInterface>;
  constructor(uri: string, options: ConnectOptions) {
    this.db = mongoose.createConnection(uri, options);
    this.db.on("error", () => {
      throw new Error("Mongo DB not connected!");
    });

    // ViewModel
    this.ViewModel = this.db.model<ViewInterface>("view", ViewSchema);
  }

  schema() {
    return {
      ViewModel: this.ViewModel,
    };
  }
}

import { Schema } from "mongoose";

export const ViewSchema = new Schema({
  user_uid: {
    type: Number,
    required: false,
  },
  episode_uid: {
    type: Number,
    required: false,
  },
  begin_time: {
    type: Number,
    required: false,
  },
  end_time: {
    type: Number,
    required: false,
  },
  created_at: {
    type: Date,
    required: false,
  },
});
